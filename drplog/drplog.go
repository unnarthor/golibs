package drplog

import (
	"go.uber.org/zap"
)

// Logger is the log instance
type Logger struct {
	Plain   *zap.Logger
	Sugared *zap.SugaredLogger
}

// L is a global instance of the Logger type
var L *Logger

// Init creates the global logger
func Init() bool {
	logger, err := zap.NewDevelopment()
	if err != nil {
		return false
	}

	sugared := logger.Sugar()

	L = &Logger{
		Plain:   logger,
		Sugared: sugared,
	}
	return true
}
